from kafka import KafkaProducer
import msgpack
import os
import json

def main():
    producer = KafkaProducer(security_protocol="PLAINTEXT",key_serializer=str.encode,bootstrap_servers=os.environ.get('KAFKA_HOST', 'localhost:9092'), value_serializer=lambda v: json.dumps(v).encode('utf-8'))
    #producer.send('foobar', key=b'foo', value=b'bar')

    
    producer.send('my-topic',key='10002', value=
        {'itemId': '10002',
            'itemName':'Milk',
            'itemPrice':100,
            'itemCount':2,
        })



if __name__ == '__main__':
    main()