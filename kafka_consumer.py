from kafka import KafkaConsumer
import json
from pymongo import MongoClient

client = MongoClient(host="localhost", port=27017)
consumer = KafkaConsumer('my-topic', group_id='test-kafka',
                         bootstrap_servers=['localhost:9092'],
                         value_deserializer=lambda m: json.loads(m.decode('utf-8')))

def main():
    db = client['test-database']
    posts = db.posts
    for message in consumer:
        post_id = posts.insert_one(message.value).inserted_id
        print ("%s:%d:%d: key=%s value=%s" % (message.topic, message.partition,
                                          message.offset, message.key,
                                          message.value))


if __name__ == '__main__':
    main()